package main

import (
	"fmt"
	"testing"
)

func TestMain(t *testing.T) {

}

func TestHandleTimeStamp(t *testing.T) {
	expected := "2011-01-01T03:00:01-05:00"
	actual, err := handleTimestamp("1/1/11 12:00:01 AM")
	checkError("Error encountered while running test", err)
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got '%s'", expected, actual)
	}
}

func TestHandleZipLengthOf2(t *testing.T) {
	expected := "00011"
	actual, err := handleZipCode("11")
	checkError("Error encountered while running test", err)
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got '%s'", expected, actual)
	}
}

func TestHandleZipLengthOf5(t *testing.T) {
	expected := "94043"
	actual, err := handleZipCode("94043")
	checkError("Error encountered while running test", err)
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got '%s'", expected, actual)
	}
}

func TestHandleNameSimpleUnicode(t *testing.T) {
	expected := "SUPERMAN ÜBERTAN"
	actual, err := handleName("Superman übertan")
	checkError("Error encountered while running test", err)
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got '%s'", expected, actual)
	}
}

func TestHandleNameNoCaseUnicode(t *testing.T) {
	expected := "株式会社スタジオジブリ"
	actual, err := handleName("株式会社スタジオジブリ")
	checkError("Error encountered while running test", err)
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got '%s'", expected, actual)
	}
}

func TestHandleDuration(t *testing.T) {
	expected := "5012.123"
	actual, err := handleDuration("1:23:32.123")
	actualString := fmt.Sprintf("%v", actual.Seconds())
	checkError("Error encountered while running test", err)
	if actualString != expected {
		t.Errorf("Test failed, expected: '%s', got '%s'", expected, actualString)
	}
}
