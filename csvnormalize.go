package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const timestampFormat = "1/2/06 3:04:05 PM"
const zipRegex = "[\\d]{5}"

func main() {
	log.Println("Beginning program")

	// Set up our reader to handle bad quotes
	reader := csv.NewReader(os.Stdin)
	reader.LazyQuotes = true

	// Set up output file to write to
	outputFile, err := os.Create("output.csv")
	checkError("Encountered error creating output file", err)
	defer outputFile.Close()

	// Set up writer
	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	// Read the file into memory for now optimize this later
	// with go channels
	index := 0
	for {
		row, err := reader.Read()
		if err == io.EOF {
			log.Println("Reached end of file")
			break
		}
		checkError("Encountered error ingesting CSV", err)

		// Skip processing header just write it out
		if index == 0 {
			index++
			writer.Write(row)
			continue
		}
		index++

		log.Printf("Processing row %d\n", index)
		row[0], err = handleTimestamp(row[0])
		if err != nil {
			log.Println("Could not convert timestamp, dropping row.")
			continue
		}
		row[2], err = handleZipCode(row[2])
		if err != nil {
			log.Println("Could not convert zipcode, dropping row.")
			continue
		}
		row[3], err = handleName(row[3])
		if err != nil {
			log.Println("Could not convert name, dropping row.")
			continue
		}
		fooDuration, err := handleDuration(row[4])
		if err != nil {
			log.Println("Could not convert duration, dropping row.")
			continue
		}
		barDuration, err := handleDuration(row[5])
		if err != nil {
			log.Println("Could not convert duration, dropping row.")
			continue
		}
		row[4] = fmt.Sprintf("%v", fooDuration.Seconds())
		row[5] = fmt.Sprintf("%v", barDuration.Seconds())
		row[6] = fmt.Sprintf("%v", fooDuration.Seconds()+barDuration.Seconds())

		writer.Write(row)
	}

	log.Println("Ending program")
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
		os.Exit(1)
	}
}

func handleTimestamp(timestamp string) (string, error) {
	// Load time zone info. the safer but less portable option
	// is to use time.FixedZone. I've opted to correct
	// for daylight savings time if necessary at the risk
	// of runtime issues when time.LoadLocation fails
	easternTime, err := time.LoadLocation("America/New_York")
	checkError("Unable to load time zone info", err)
	pacificTime, err := time.LoadLocation("America/Los_Angeles")
	checkError("Unable to load time zone info", err)
	// Parse the time
	parsedTime, err := time.ParseInLocation(timestampFormat, timestamp, pacificTime)
	if err != nil {
		return timestamp, err
	}

	timeInEastern := parsedTime.In(easternTime)

	return timeInEastern.Format(time.RFC3339), nil
}

func handleZipCode(zip string) (string, error) {
	isFiveDigits, err := regexp.MatchString(zipRegex, zip)
	if err != nil {
		return zip, err
	}

	if isFiveDigits {
		return zip, nil
	}

	parsedZip, err := strconv.Atoi(zip)
	if err != nil {
		return zip, err
	}

	return fmt.Sprintf("%05d", parsedZip), nil
}

func handleName(name string) (string, error) {
	return strings.ToUpper(name), nil
}

func handleDuration(duration string) (time.Duration, error) {
	segments := strings.Split(duration, ":")
	secondsMillis := strings.Split(segments[2], ".")
	parseableString := fmt.Sprintf("%vh%vm%vs%vms", segments[0], segments[1], secondsMillis[0], secondsMillis[1])
	parsedDuration, err := time.ParseDuration(parseableString)
	if err != nil {
		return time.Since(time.Now()), err
	}
	return parsedDuration, nil
}
