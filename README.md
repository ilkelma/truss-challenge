# Summary of efforts

When I saw csv and utf-8 I immediately thought of Go because of its built in utf-8 support. I would normally choose a language like ruby or python for a task like this but they have a variety of quirks when dealing with utf-8 strings - python especially. I also wanted to do this with only using the standard library and Go has an excellent standard library. I'm not a total expert at Go so there may be some coding that isn't entirely idiomatic (though thankfully gofmt exists to force some of that)

I had planned to also leverage go concurrency to parse the csv in parallel but unfortuantely I ran out of time (there's a comment marking where I'd planned to do this).

## To build
This was written with go 1.10 but I don't believe it relies on any features from that version in particular. I'd say it's compatible with many versions.

In order to build it should be as simple as `go build` on a machine with the go toolchain installed. The only dependencies are the standard lib so it shouldn't need `go get`.

You can also test it with `go test -v` if you'd like to see the tests I wrote while working on it.

Alternatively the latest versions of the binaries are built by gitlab CI for both platforms mentioned here:

- [linux-amd64](https://gitlab.com/ilkelma/truss-challenge/-/jobs/artifacts/master/raw/truss-challenge?job=build:linux)
- [Mac/Darwin-amd64](https://gitlab.com/ilkelma/truss-challenge/-/jobs/artifacts/master/raw/truss-challenge?job=build:mac)

I should of course point out that all the ci build stuff was not accounted for in the 4 hour time box. I just thought it would be convenient to have the binaries to simply run. This write-up was also not part of the time box but things like the tests and any in code comments were.